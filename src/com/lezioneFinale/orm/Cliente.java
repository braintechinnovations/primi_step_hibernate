package com.lezioneFinale.orm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cliente")
public class Cliente {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cliente")
	private int clienteid;

	@Column(name="nome")
	private String nome;

	@Column(name="cognome")
	private String cognome;
	
	@Column(name="numero_tel")
	private String numtelefono;
	
	@Column(name="codice_cli")
	private String codice;

	public int getClienteid() {
		return clienteid;
	}

	public void setClienteid(int clienteid) {
		this.clienteid = clienteid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNumtelefono() {
		return numtelefono;
	}

	public void setNumtelefono(String numtelefono) {
		this.numtelefono = numtelefono;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	@Override
	public String toString() {
		return "Cliente [clienteid=" + clienteid + ", nome=" + nome + ", cognome=" + cognome + ", numtelefono="
				+ numtelefono + ", codice=" + codice + "]";
	}
}
