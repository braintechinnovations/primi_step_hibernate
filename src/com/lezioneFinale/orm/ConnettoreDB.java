package com.lezioneFinale.orm;

import java.sql.Connection;
import java.sql.SQLException;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConnettoreDB {
	
	private SessionFactory factory;
	private static ConnettoreDB ogg_connessione;
	
	public static ConnettoreDB getIstanza() {
		if(ogg_connessione == null) 
			ogg_connessione = new ConnettoreDB();
		
		return ogg_connessione;
	}
	
	public SessionFactory getSessione() throws SQLException{
		if(factory == null) {
			factory = new Configuration()
								.configure("com/lezioneFinale/orm/resources/hibernate.cfg.xml")
								.addAnnotatedClass(Cliente.class)
								.buildSessionFactory();
		}
		
		return factory;
	}
}
