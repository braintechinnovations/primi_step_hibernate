package com.lezioneFinale.orm;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class Main {

	public static void main(String[] args) {

		try {
			SessionFactory factory = ConnettoreDB.getIstanza().getSessione();
			
			//Inserimento cliente
//			Cliente cli_1 = new Cliente();
//			cli_1.setNome("Giovanni");
//			cli_1.setCognome("Pace");
//			cli_1.setNumtelefono("123456");
//			cli_1.setCodice("CLI999");
//			
//			Cliente cli_2 = new Cliente();
//			cli_2.setNome("Mario");
//			cli_2.setCognome("Rossi");
//			cli_2.setNumtelefono("54321");
//			cli_2.setCodice("CLI998");
//			
//			
//			Session sessione = factory.getCurrentSession();
//			sessione.beginTransaction();
//			
//			sessione.save(cli_1);
//			sessione.save(cli_2);
//			
//			sessione.getTransaction().commit();
			
			//Selezione di tutti i campi
//			Session sessione = factory.getCurrentSession();
//			sessione.beginTransaction();
//			List<Cliente> elenco = sessione.createQuery("FROM Cliente").list();
//			sessione.getTransaction().commit();
//			
//			for(Cliente c : elenco) {
//				System.out.println(c.toString());
//			}
			

			Session sessione = factory.getCurrentSession();
			sessione.beginTransaction();			
			int affRows = sessione.createQuery("DELETE Cliente WHERE clienteid = 10").executeUpdate();
			sessione.getTransaction().commit();	
			
			System.out.println(affRows);
			
			sessione.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		
		
	}

}
